#!/usr/bin/python

# Copyright: (c) 202!, Abel Luck <abel@guardianproject.info>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: uptime

short_description: Gets the system uptime in seconds

version_added: "1.0.0"

description: Gets the system uptime in seconds, but not as a fact. Designed to be used when gather_facts is false.

options:

author:
    - Abel Luck (@abelxluck)
'''

EXAMPLES = r'''
- name: get uptime
  gpops.common.uptime:
'''

RETURN = r'''
uptime_seconds:
    description: The system uptime in seconds
    type: integer
    returned: always
    sample: 42
'''

from ansible.module_utils.basic import AnsibleModule


def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict()

    result = dict(
        changed=False,
        original_message='',
        message='',
        uptime_seconds=None,
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    with open('/proc/uptime', 'r') as f:
        uptime_float = float(f.readline().split()[0])

    uptime_seconds = int(uptime_float)
    # manipulate or modify the state as needed (this is going to be the
    # part where your module will do what it needs to do)
    result['uptime_seconds'] = uptime_seconds
    module.exit_json(**result)

def main():
    run_module()


if __name__ == '__main__':
    main()

