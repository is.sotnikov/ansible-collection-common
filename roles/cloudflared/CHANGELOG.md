# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.3.0](https://gitlab.com/guardianproject-ops/ansible-cloudflared/compare/0.2.2...0.3.0) (2021-06-11)


### ⚠ BREAKING CHANGES

* Remove cloudflared_service_state variable

### Bug Fixes

* Remove cloudflared_service_state variable ([40403dd](https://gitlab.com/guardianproject-ops/ansible-cloudflared/commit/40403ddc9513bbcee9bdd12dbcc32aa53c11faa1))

### [0.2.2](https://gitlab.com/guardianproject-ops/ansible-cloudflared/compare/0.2.1...0.2.2) (2021-02-09)


### Bug Fixes

* Remove unnecessary Ansible block ([3c21d07](https://gitlab.com/guardianproject-ops/ansible-cloudflared/commit/3c21d07bc156dd121e09faad8de651cc72e1e644))

### [0.2.1](https://gitlab.com/guardianproject-ops/ansible-cloudflared/compare/0.2.0...0.2.1) (2021-02-09)


### Features

* Install .deb from Cloudflare's package repository ([ab3117a](https://gitlab.com/guardianproject-ops/ansible-cloudflared/commit/ab3117aded9f45e36b625a3aaff430d51d8f11c7))

## [0.2.0](https://gitlab.com/guardianproject-ops/ansible-cloudflared/compare/0.1.2...0.2.0) (2021-01-27)


### ⚠ BREAKING CHANGES

* update cloudflared and allow pinning
* use new Named Argo Tunnels

### Features

* rotate the cloudflared log files ([34926bd](https://gitlab.com/guardianproject-ops/ansible-cloudflared/commit/34926bd2a1d49b243a67d9c991f7976c2ca504a8))
* update cloudflared and allow pinning ([51172a7](https://gitlab.com/guardianproject-ops/ansible-cloudflared/commit/51172a7aec255a6644314f5e6dcae5999a73e0cb))
* use new Named Argo Tunnels ([46bc724](https://gitlab.com/guardianproject-ops/ansible-cloudflared/commit/46bc724f7cad3f168965b8452dd328bb7e308122))


### Bug Fixes

* catch even more instance of secrets leaking ([0a947e6](https://gitlab.com/guardianproject-ops/ansible-cloudflared/commit/0a947e63b742afad378ca765cab588ef3de2a6a7))
* ensure tunnel secret is not logged ([11635ac](https://gitlab.com/guardianproject-ops/ansible-cloudflared/commit/11635acfc9fcda1490c4032dc7dd0e442da92d7d))
* use absolute path to credentials file ([1dbbcc7](https://gitlab.com/guardianproject-ops/ansible-cloudflared/commit/1dbbcc7fdb7cc28eeec3e7b972d996e4bd2a6d68))

### [0.1.2](https://gitlab.com/guardianproject-ops/ansible-cloudflared/compare/v0.1.1...v0.1.2) (2020-09-30)

### Features

* expose cloudflared metrics ([8b419a1](https://gitlab.com/guardianproject-ops/ansible-cloudflared/commit/8b419a104249fbef3b91e093faad4869bb322b39))

### 0.1.1 (2020-09-18)


### Bug Fixes

* use new assert style validation ([c790be6](https://gitlab.com/guardianproject-ops/ansible-cloudflared/commit/c790be6369b2c1f43f0a9277aa93f3b6a96ff70c))

## [0.1.0] - 2020-04-17

### Added

- This CHANGELOG

### Changed

n/a

### Removed

n/a

[Unreleased]: https://gitlab.com/guardianproject-ops/ansible-ntp/compare/0.1.0...master
[0.1.0]: https://gitlab.com/guardianproject-ops/ansible-ntp/-/tags/0.1.0
