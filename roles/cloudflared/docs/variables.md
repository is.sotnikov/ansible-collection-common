## Role Variables

* `cloudflared_base_url`: `"https://bin.equinox.io/c/VdrWdbjqyF/"` - upstream url to download cloudflared from



* `cloudflared_amd64_apt`: `"cloudflared-stable-linux-amd64.deb"` - the name of the deb package to install



* `cloudflared_bin_location`: `/usr/local/bin` - the location the cloudflared binary will be placed



* `cloudflared_enable_service`: `true` - whether the cloudflared argo tunnel service will be enabled



* `cloudflared_tunnels`: `[]` - the tunnels to create


  cloudflared_tunnels:
    - tunnel_name: foo
      tunnel_id: uuid
      tunnel_secret: the shared secret
      tunnel_upstream: http://127.0.0.1:8001
      ingress:
      - service: http://127.0.0.1:8001
        hostname: yourapp.foo.com
        path: optional, see docs
      metrics_bind: 0.0.0.0:9300 (default is 'localhost:' which binds to a random port, not very useful)
  note: ingress and tunnel_upstream are mutually exclusive

* `cloudflare_account_id`: `` - the cloudflare account id



* `cloudflared_pin_version`: `` - a version string passed to `cloudflare update --version` that will pin the version


