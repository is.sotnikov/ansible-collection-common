---
- name: Install packages
  ansible.builtin.apt:
    name:
      - unattended-upgrades
      - apt-listchanges
    state: present
    install_recommends: false
  when: unattended_upgrades_enabled | bool

- name: Configure debconf response
  ansible.builtin.debconf:
    name: unattended-upgrades
    question: unattended-upgrades/enable_auto_updates
    vtype: boolean
    value: '{{ "true" if unattended_upgrades_enabled|bool else "false" }}'

- name: Configure periodic APT updates
  ansible.builtin.template:
    src: 20periodic.j2
    dest: /etc/apt/apt.conf.d/20periodic
    owner: root
    group: root
    mode: 0644
  when: unattended_upgrades_enabled | bool

- name: Configure periodic APT upgrades
  ansible.builtin.template:
    src: 20auto-upgrades.j2
    dest: /etc/apt/apt.conf.d/20auto-upgrades
    owner: root
    group: root
    mode: 0644
  when: unattended_upgrades_enabled | bool

- name: Divert unattended-upgrades configuration
  ansible.builtin.command: dpkg-divert --quiet --local --divert /etc/apt/apt.conf.d/50unattended-upgrades.dpkg-divert
           --rename /etc/apt/apt.conf.d/50unattended-upgrades
  args:
    creates: /etc/apt/apt.conf.d/50unattended-upgrades.dpkg-divert
  when: unattended_upgrades_enabled | bool

- name: Configure unattended-upgrades
  ansible.builtin.template:
    src: 50unattended-upgrades.j2
    dest: /etc/apt/apt.conf.d/50unattended-upgrades
    owner: root
    group: root
    mode: 0644
  when: unattended_upgrades_enabled | bool

- name: Remove configs out of the way before reversion
  ansible.builtin.command: rm -f /etc/apt/apt.conf.d/50unattended-upgrades
  args:
    removes: /etc/apt/apt.conf.d/50unattended-upgrades.dpkg-divert
    warn: false
  when: not unattended_upgrades_enabled | bool

- name: Revert unattended-upgrades configuration
  ansible.builtin.command: dpkg-divert --quiet --local --rename
           --remove /etc/apt/apt.conf.d/50unattended-upgrades
  args:
    removes: /etc/apt/apt.conf.d/50unattended-upgrades.dpkg-divert
  when: not unattended_upgrades_enabled | bool

- name: Configure apt-listchanges
  ansible.builtin.template:
    src: listchanges.conf.j2
    dest: /etc/apt/listchanges.conf
    owner: root
    group: root
    mode: 0644
  when: unattended_upgrades_listchanges_enabled | bool

- name: Remove apt-listchanges config if disabled
  ansible.builtin.file:
    path: /etc/apt/listchanges.conf
    state: absent
  when: not unattended_upgrades_listchanges_enabled | bool
